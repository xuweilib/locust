import asyncio
import time


async def tcp_echo_client(data, events):
    start_time = time.time()
    reader, writer = await asyncio.open_connection(
        '192.168.8.89', 9102)

    # print(f'Send: {message!r}')
    # writer.write(message.encode())
    # data = await reader.read(100)
    # print(f'Received: {data.decode()!r}')
    # send_data = "7E02000026012345678912000400000000000000030261136106F0124D0058029400002312110305350104000004CE02020000127E"
    writer.write(bytes.fromhex(data))
    data = await reader.read(100)
    print(data)  # 打印
    # print('Close the connection')
    # writer.close()
    # await writer.wait_closed()

    total_time = int((time.time() - start_time) * 1000)
    events.request.fire(
        request_type="tcp",
        name=f"send_",
        response_time=total_time,
        response_length=len(data),
        exception=None,
        context=None)


if __name__ == '__main__':
    asyncio.run(tcp_echo_client)
