import asyncio
import time
import socket
from random import random

# from locust import User, TaskSet, events, task
from locust import TaskSet, task, events, User, between, constant

from TcpClient import tcp_echo_client

"""

 参考地址:
 1、https://www.cnblogs.com/jumping0709/p/15687877.html
 2、https://zhuanlan.zhihu.com/p/616587336
 
"""


class TcpUser(User):

    def __init__(self, *args, **kwargs):
        super(TcpUser, self).__init__(*args, **kwargs)
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((self.host, self.port))

    def on_stop(self):
        self.client.close()


class TestTaskSet(TaskSet):
    # constant(2)  # 任务执行完毕等待2秒开始下一任务。
    # between(1, 7)  # 任务执行完毕等待1-7秒（中间随机取值）开始下一任务
    # constant_pacing(2)  # 设置任务启动总得等待时间，若任务耗时超过该时间，则任务结束后立即执行下一任务；若任务耗时不超过该时间，则等待达到该时间后执行下一任务。

    # wait_time = between(2, 5)
    # wait_time = constant(1)
    @task
    def async_send_recv_data(self):

        tcp_echo_client("5B33472A383031303135333034312A303030392A4C4B2C302C302C31395D",events)

    # @task  ##里面参数为权重，值越大，权重越大
    # def send_data(self):
    #     start_time = time.time()
    #     try:
    #         send_data = "5B33472A383031303135333034312A303030392A4C4B2C302C302C31395D"
    #         # send_data = "7E02000026012345678912000400000000000000030261136106F0124D0058029400002312110305350104000004CE02020000127E"
    #         self.client.send(bytes.fromhex(send_data))
    #         print(send_data)  # 打印
    #         recv_data = self.client.recv(128)
    #         print(recv_data)  # 回显
    #     # 成功、失败判断逻辑
    #     except Exception as e:
    #         total_time = int((time.time() - start_time) * 1000)
    #         events.request.fire(
    #             request_type="tcp",
    #             name=f"send_",
    #             response_time=total_time,
    #             response_length=0,
    #             exception=e,
    #             context=None)
    #     else:
    #         total_time = int((time.time() - start_time) * 1000)
    #         events.request.fire(
    #             request_type="tcp",
    #             name=f"send_",
    #             response_time=total_time,
    #             response_length=len(send_data),
    #             exception=None,
    #             context=None)

    # @task  ##里面参数为权重，值越大，权重越大
    # def recv_data(self):
    #     start_time = time.time()
    #     try:
    #         recv_data = self.client.recv(128)
    #         print(recv_data)  # 回显
    #     # 成功、失败判断逻辑
    #     except Exception as e:
    #         total_time = int((time.time() - start_time) * 1000)
    #         events.request.fire(
    #             request_type="tcp",
    #             name="sendRecv",
    #             response_time=total_time,
    #             response_length=0,
    #             exception=e,
    #             context=None)
    #     else:
    #         total_time = int((time.time() - start_time) * 1000)
    #         events.request.fire(
    #             request_type="tcp",
    #             name="sendRecv",
    #             response_time=total_time,
    #             response_length=len(recv_data),
    #             exception=None,
    #             context=None)


class TcpTestUser(TcpUser):
    tasks = [TestTaskSet]
    host = "192.168.8.89"
    port = 8100


if __name__ == "__main__":
    import os

    file_path = os.path.abspath(__file__)
    os.system(f'locust -f  {file_path} --web-port=1999 ')  ##请求地址一定要配IP

    """
    
    测试地址：
        1、http://localhost:9999/
    """
