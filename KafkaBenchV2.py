import time
# from locust import TaskSet, task, Locust, events

from locust import TaskSet, task, events, User, between, constant
from kafka import KafkaProducer
import json


class UserBehavior(TaskSet):
    # def __init__(self, parent: "User"):
    #     super().__init__(parent)
    #     self.producer = None

    def __init__(self, parent: "User"):
        super().__init__(parent)
        self.producer = None

    def on_start(self):

        self.producer = KafkaProducer(bootstrap_servers=['192.168.16.100:9092'],
                                      key_serializer=lambda k: json.dumps(k).encode(),
                                      value_serializer=lambda v: json.dumps(v).encode())

    def on_stop(self):
        # 该方法当程序结束时每用户进行调用，关闭连接
        self.producer.close()

    @task(1)
    def sendAddCmd(self):
        start_time = time.time()
        # time_unique_id = time.strftime("%Y-%m-%d-%H-%M-%S", time.localtime())
        # print(time_unique_id)
        # print("===========================", start_time)
        try:
            start_time = int(time.time())
            message = {
                'timestamp': start_time,
                'message': "121314"
            }
            msg = json.dumps(message)
            msg = msg.encode('utf-8')
            self.producer.send('preHandleTopic', msg)
            print(msg)
        except Exception as e:
            print(e)
            total_time = int((time.time() - start_time) * 1000)
            events.request.fire(
                request_type="kafka",
                name=f"add",
                response_time=total_time,
                response_length=len(message),
                exception=e,
                context=None)

        else:
            # total_time = int((time.time() - start_time) * 1000)
            # events.request_success.fire(request_type="kafka", name="add", response_time=total_time,
            #                             response_length=0)
            total_time = int((time.time() - start_time) * 1000)
            events.request.fire(
                request_type="kafka",
                name=f"add",
                response_time=total_time,
                response_length=len(message),
                exception=None,
                context=None)


class SocketUser(User):
    # bootstrap_servers = '192.168.16.100:9092'
    # producer_settings = {'acks': 1}
    tasks = [UserBehavior]
    host = "192.168.8.89"
    # min_wait = 1000  # 单位毫秒
    # max_wait = 1000  # 单位毫秒


if __name__ == "__main__":
    import os

    file_path = os.path.abspath(__file__)
    os.system(f'locust -f  {file_path} --web-port=1999 ')  ##请求地址一定要配IP

    """

    测试地址：
        1、http://localhost:9999/
    """
