from locust_plugins.users.kafka import KafkaUser
from locust import task, TaskSet
import json
import time

class MyTaskSet(TaskSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user_id = None  # 存储当前用户ID
        self.num = 0

    @task
    def my_task(self):
        if self.user_id is None:
            self.user_id = MyUser.current_user_id
            MyUser.current_user_id += 1
        while True:
            msg = {
                "uuid": "603F447B37D341fd88D8CBB8945EFE79",
                "callid": "NTA5YTA0NTMyZjExY2Y5Y2MxMmIwY2NiYjRmOTIxOGE.",
                "caller": str(self.user_id),
                "callee": "99999",
                "direction": 1,
                "seq": 8,
                "sendTime": 1667294609.829818,
                "spendTime": 20,
                "voiceTime": 4500,
                "new": False,
                "eof": False,
                "recPath": "",
                "timestamp": time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()),
                "text": f"{str(self.user_id)}第{self.num}次说现在增值税税率是多少"
            }
            msg_str = json.dumps(msg, ensure_ascii=False)
            self.client.send("uasr-assist-events", msg_str.encode('utf-8'))

            print(f"{str(self.user_id)}第{self.num}次说现在增值税税率是多少")
            self.num += 1
            # time.sleep(0.2)


class MyUser(KafkaUser):
    bootstrap_servers = '192.168.16.100:9092'
    producer_settings = {'acks': 1}
    current_user_id = 7000
    tasks = [MyTaskSet]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


# if __name__ == "__main__":
#     run_single_user(MyUser)

if __name__ == "__main__":
    import os

    file_path = os.path.abspath(__file__)
    os.system(f'locust -f  {file_path} --web-port=1999 ')  ##请求地址一定要配IP

    """

    测试地址：
        1、http://localhost:9999/
    """
