## 运行说明
1. Linux 本地运行： locust -f locust_file.py --no-web -c10 -r10
2. Window的pycharm运行
   - 脚本如下会自动运行 os.system(f'locust -f  {file_path} --web-port=1999 ')  ##请求地址一定要配IP 
3. 依赖安装
   - paho-mqtt
   - kafka-python
   - locust
4. Window安装
   安装Python3.11 高的版本，一次性成功
   PIP改用豆瓣源
   window 在用户 c:/user/admin新建pip目录。下面建pip.ini
   [global]
   timeout = 6000
   index-url=http://pypi.douban.com/simple
   [install]
   trusted-host=pypi.douban.com
   locust -f xx.py运行压力测试

