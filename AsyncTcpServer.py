from datetime import datetime
import asyncio


# 服务器的回调函数
async def script_handle(reader, writer):  # reader和writer参数是asyncio.start_server生成异步服务器后自动传入进来的
    while True:  # 循环接受数据，直到套接字关闭
        # wait_for等待读取数据，第二个参数为等待时间(None表示无限等待)
        data = await asyncio.wait_for(reader.read(2 ** 10), None)
        if not data:
            print('script client disconnected')
            writer.close()  # 关闭套接字
            await writer.wait_closed()  # 等待套接字完全关闭
            return
        print("received data: ", data.decode())
        writer.write(data.encode())  # 发送数据
        await writer.drain()  # 发送数据后，清空套接字


# 主函数
async def main():
    # 生成一个服务器
    server = await asyncio.start_server(script_handle,
                                        host='192.168.8.89',
                                        port=8888)

    # 获取请求连接的客户端信息
    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')
    # 处理多个请求，永远执行着调用
    async with server:
        await server.serve_forever()


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print(datetime.now().strftime('%Y/%m/%d %H:%M:%S.%f'),
              'script server exit by key')
